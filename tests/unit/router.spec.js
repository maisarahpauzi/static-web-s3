import { shallowMount, createLocalVue } from '@vue/test-utils'
import VueRouter from 'vue-router'
import Home from '../../src/views/Home.vue'
import About from '../../src/views/About.vue'
import router from "../../src/router"

const localVue = createLocalVue()
localVue.use(VueRouter)

describe("About", () => {
  it("should change page", async () => {
    const routes = new VueRouter({ router })
    const wrapper = shallowMount(About, { 
      localVue,
      routes
    })

    router.push("/about")
    await wrapper.vm.$nextTick()

    expect(wrapper.findComponent(About).exists()).toBe(true)
  })
})